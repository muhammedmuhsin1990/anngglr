import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http,RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { User, ApiUsers } from './user.model';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'jwt-token'
    })
  };

@Injectable()
export class UsersService {

    option:any;
    headers: Headers;
    public url = "api/users";
    constructor(public http:HttpClient) {
        this.headers = new Headers({"Content-Type":"application/json"});
        this.option = httpOptions;
     }
    
    getUsers(): Observable<User[]> {
        return this.http.get<User[]>(this.url);
    }

    addUser(user:User){	    
        return this.http.post(this.url, user);
    }

    updateUser(user:User){
        return this.http.put(this.url, user);
    }

    deleteUser(id: number) {
        return this.http.delete(this.url + "/" + id);
    } 

        testApi(){
        return this.http.get('https://jsonplaceholder.typicode.com/users',httpOptions)
    }
} 